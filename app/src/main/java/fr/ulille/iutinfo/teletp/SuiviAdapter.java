package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder>/* TODO Q6.a */ {

    private List<String> questions;
    private LayoutInflater mInflater;

    public SuiviAdapter(SuiviViewModel model) {
        this.mInflater = LayoutInflater.from(model.getApplication().getApplicationContext());
        this.questions = Arrays.asList(model.getQuestions());

    }

    // construit les lignes à partir du xml quand c'est necessaire
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }
    // relie les données au textView dans chaque ligne
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String salle= questions.get(position);
        holder.myTextView.setText(salle);
    }
    // nombre total de ligne
    @Override
    public int getItemCount() {return questions.size();}
    // stocke et reafiche les views au fur et à mesure du scrolling sur l'écran
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.question);
        }
    }
    // TODO Q6.a
    // TODO Q7
}
