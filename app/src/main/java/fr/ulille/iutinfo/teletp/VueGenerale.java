package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    public String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;

    private Spinner spSalle;
    private Spinner spPoste;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        poste="";
        salle=DISTANCIEL;

        // TODO Q2.c
        this.model=MainActivity.getModel();

        // TODO Q4
        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes, android.R.layout.simple_spinner_item);
        spPoste = (Spinner) view.findViewById(R.id.spPoste);
        // Apply the adapter to the spinner
        spPoste.setAdapter(adapterPoste);
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles, android.R.layout.simple_spinner_item);
        spSalle = (Spinner) view.findViewById(R.id.spSalle);
        // Apply the adapter to the spinner
        spSalle.setAdapter(adapter);
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView login = view.findViewById(R.id.tvLogin);
            model.setUsername(login.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        // TODO Q9
    }

    // TODO Q5.a
    public void update(){

        if(spSalle.getSelectedItem().toString().equals("Distanciel")){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            salle="Distanciel";
            poste="";
            model.setLocalisation("Distanciel");
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            salle=spSalle.getSelectedItem().toString();
            poste=spPoste.getSelectedItem().toString();
            model.setLocalisation(""+salle+" : "+poste);
        }
    }
    // TODO Q9
}